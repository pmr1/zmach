        include "oz.def"
        include "vmstate.def"

        module mem

        xdef meminit, memfinish

.meminit
        ;; allocate pool for memory
        ld      a, MM_S1                          ; will load it in segment 1 (zero is intutition)
        ld      b, 0                              ; 'always'
        CALL_OZ(OS_Mop)
        jp      C, meminit_done

        ;; memory pool id is now in IX, store it in the vmstate
        ld      (VMSTAT_BASE+vm_poolid), ix

        ld      a,1
        call    make_section

        jp      meminit_done

.closemem
        CALL_OZ(OS_Mcl)                           ; close memory pool in IX

.meminit_done
        ret

.memfinish
        call destroy_section
        ret

;;;
;;; Memory consists of 'SECTIONS' which are made up of 'SEGMENTS'.
;;;
;;; A SEGMENT is 32 x 256 byte pages = 8k
;;;
;;; The seg_table is an array of 5 bytes for each page in the
;;; segment. The page entry contains:
;;;
;;;   + base extended addr of the 256 bytes of memory assigned to the page [3 bytes]
;;;   + pool id from which the memory was allocated [2 bytes] (required for freeing it)
;;;
;;;
;;; A SECTION is a 1 byte count of the number of segments,
;;; followed by a list of extended base addresses for segment
;;; tables. This section table should fit into a a single OZ
;;; page of 256 bytes, making the maxium size of a section
;;; 255/3=85 segements, or 680k (i.e., a lot).

;;; ***********************************************************************************
;;;
;;; make_section
;;;
;;; IN: A -> number of segments to insert into the section
;;;
;;; DESTROYS: B

.make_section

        ;; here we will need to allocate room for the section table
        ld      c,a
        add     a,a
        add     a,c                               ; multiply by three (bytes per entry)

        ld      c,a
        ld      d,a                               ; save size (with a spare in d)
        ld      a,0
        ld      ix, (VMSTAT_BASE+vm_poolid)       ; pool id for mem alloc
        CALL_OZ(OS_Mal)

        jp      c,make_section_end

        ;; successful: store the address for the base of the section table in the status struct
        ld      (VMSTAT_BASE+vm_section_table), hl   ; address      \____ extended addr
        ld      (VMSTAT_BASE+vm_section_table_pool), ix
        push    hl
        ld      hl, VMSTAT_BASE+vm_section_table+2   ;              /
        ld      (hl), b                              ; bank number /
        ld      hl, VMSTAT_BASE+vm_section_table_sz
        ld      (hl), d

        pop hl

        ;; bind new segment
        ld      c, MS_S1
        ; b already contains bank
        CALL_OZ(OS_Mpb)
        jp      c, make_section_err

.make_section_zero_loop
        ld      (hl), 0
        inc     hl
        dec     d
        jp      nz,make_section_zero_loop

        ld      a, 0                              ; success

.make_section_end
        ret

.make_section_err
        ld      c, d                              ; size of mem
        ld      d, a                              ; save prev err vode
        ld      a,b                               ; bank
        ld      b,0                               ; sz is 16-bits in BC
        ld      ix, (VMSTAT_BASE+vm_poolid)
        ;; hl still base addr

        CALL_OZ(OS_Mfr)
        ld      a, d                              ; restore err code
        scf                                       ; set carry flag to indicate err
        jp      make_section_end
;;; ********************************************************************************

;;; destroy_section:
;;;     frees the memory associated with a section
;;;     TODO: currently this just frees the section table, but it
;;;     should also be freeing the actual memory. Also, I don't think
;;;     I'm storing the pool id that is specifically assocated with
;;;     the section table.

.destroy_section
        ld      ix, (VMSTAT_BASE+vm_section_table_pool) ; grab the pool that this was allocated from
        ld      hl, (VMSTAT_BASE+vm_section_table)      ; base address of the table
        ld      a,  (VMSTAT_BASE+vm_section_table_sz)   ; amount of memory that was assigned to it
        ld      c,  a
        ld      b,  0                                   ; size is 16-bit in reg BC
        ld      a,  (VMSTAT_BASE+vm_section_table+2)    ; bank (from extended addr)
        CALL_OZ(OS_Mfr)
        ; keep flags and passthrough error if there was one
        ret
