DEBUG_BIN=../../ext/z88/z88apps/z88apps/intuition/debugS00.bin

APP=zmach

OBJS=main.obj mem.obj

# ASM_DEFS=-DDEBUG
ASM_DEFS=-DMPM -DDEBUG

ASM=mpm
# ASM=z80asm

OZVM_JAR=$(HOME)/src/ext/z88/ozvm/z88.jar

$(APP).epr: $(APP).bin cardhdr.bin
	z88card -szc 32 $@ $(DEBUG_BIN) 3e0000 $(APP).bin 3f0000 cardhdr.bin 3f3fc0

cardhdr.bin: cardhdr.asm
	$(ASM) $(ASM_DEFS) -b $<

$(APP).bin: $(OBJS)
	$(ASM) -cz80 -o$@ $(ASM_DEFS) -b $(OBJS)

%.obj:	%.asm
	$(ASM) $(ASM_DEFS) -j $<

clean:	
	rm -vrf *.bin $(APP).6? *.map

run:	zmach.epr
	java -jar $(OZVM_JAR) crd2 1024 29f $(APP).epr
