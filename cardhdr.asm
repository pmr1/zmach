        module CARD_HEADER

        org $3FC0

        ;;; application front-dor

.appl_front_dor
        defb    0, 0, 0                           ; link to parent...
        defb    0, 0, 0                           ; no help DOR
        defw    $C000                             ; pointer to Application DOR (bottom of bank)
        defb    $3F                               ; in top bank
        defb    $13                               ; DOR type - ROM front DOR
        defb    8                                 ; length of DOR
        defb    'N'
        defb    5                                 ; length of name and terminator
        defm    "APPL", 0
        defb    $FF                               ; end of application front DOR

        defs    37                                ; blanks to fill-out space.

.eprom_header
        defw    $0000                             ; $3FF8 Card ID, to be filled in by loadmap
        defb    @00000011                         ; $3FFA Country Code
        defb    $80                               ; $3FFB external application
        defb    $01                               ; $3FFC size of EPROM (1 banks of 16K = 16K)
        defb    0                                 ; $3FFD subtype of card ...
        defm    "OZ"                              ; $3FFE card is an application EPROM

.EpromTop
